-- begin DOCUMENTS_WORKER
create table DOCUMENTS_WORKER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    PERSONAL_NUMBER varchar(255) not null,
    USER_ID uuid,
    FIRST_NAME varchar(255) not null,
    LAST_NAME varchar(255) not null,
    PATRONYMIC varchar(255),
    DEPARTMENT_ID uuid not null,
    EMAIL varchar(255),
    PHONE varchar(255),
    PHOTO uuid,
    --
    primary key (ID)
)^
-- end DOCUMENTS_WORKER
-- begin DOCUMENTS_DOCUMENT_TYPE
create table DOCUMENTS_DOCUMENT_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CODE varchar(255),
    NAME varchar(255),
    --
    primary key (ID)
)^
-- end DOCUMENTS_DOCUMENT_TYPE
-- begin DOCUMENTS_OUTGOING_DOCUMENT
create table DOCUMENTS_OUTGOING_DOCUMENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TYPE_ID uuid not null,
    REGISTRATION_NUMBER varchar(255),
    REGISTRATION_DATE timestamp,
    ADDRESSEE varchar(255),
    SUBJECT varchar(255) not null,
    ADDRESSEE_COMPANY varchar(255),
    EXECUTOR_WORKER_ID uuid,
    SIGNATORY_WORKER_ID uuid,
    NOTE varchar(255),
    NAME varchar(255),
    AUTHOR_USER_ID uuid not null,
    CREATION_DATE timestamp,
    EDITED_DATE timestamp,
    STATUS varchar(255),
    APPROVER_ID uuid,
    TASK_START_DATE timestamp,
    TASK_COMPLETION_DATE timestamp,
    RESULT_ varchar(255),
    COMMENT_ varchar(255),
    --
    primary key (ID)
)^
-- end DOCUMENTS_OUTGOING_DOCUMENT
-- begin DOCUMENTS_DEPARTMENT
create table DOCUMENTS_DEPARTMENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    CODE varchar(255) not null,
    NAME varchar(255) not null,
    LEADING_DEPARTMENT_ID uuid,
    HEAD_ID uuid,
    --
    primary key (ID)
)^
-- end DOCUMENTS_DEPARTMENT
-- begin DOCUMENTS_OUTGOING_DOCUMENT_FILE_DESCRIPTOR_LINK
create table DOCUMENTS_OUTGOING_DOCUMENT_FILE_DESCRIPTOR_LINK (
    OUTGOING_DOCUMENT_ID uuid,
    FILE_DESCRIPTOR_ID uuid,
    primary key (OUTGOING_DOCUMENT_ID, FILE_DESCRIPTOR_ID)
)^
-- end DOCUMENTS_OUTGOING_DOCUMENT_FILE_DESCRIPTOR_LINK
