create table DOCUMENTS_WORKER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    PERSONAL_NUMBER varchar(255) not null,
    USER_ID uuid,
    FIRST_NAME varchar(255),
    LAST_NAME varchar(255),
    PATRONYMIC varchar(255),
    DEPARTMENT_ID uuid not null,
    EMAIL varchar(255),
    PHONE varchar(255),
    PHOTO uuid,
    --
    primary key (ID)
);