update DOCUMENTS_WORKER set FIRST_NAME = '' where FIRST_NAME is null ;
alter table DOCUMENTS_WORKER alter column FIRST_NAME set not null ;
update DOCUMENTS_WORKER set LAST_NAME = '' where LAST_NAME is null ;
alter table DOCUMENTS_WORKER alter column LAST_NAME set not null ;
