package ru.pautova.documents.core;

import com.haulmont.bpm.core.ProcessRepositoryManagerBean;
import com.haulmont.bpm.entity.ProcDefinition;
import com.haulmont.bpm.entity.ProcModel;
import com.haulmont.bpm.entity.ProcRole;
import com.haulmont.bpm.exception.BpmException;
import com.haulmont.bpm.exception.InvalidModelException;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Transaction;
import org.activiti.bpmn.exceptions.XMLException;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

import javax.annotation.Nullable;
import java.util.List;

public class ExtProcessRepositoryManagerBean extends ProcessRepositoryManagerBean {

    @Override
    public ProcDefinition deployProcessFromXml(String xml, @Nullable ProcDefinition procDefinition, @Nullable ProcModel procModel) {
        Transaction tx = persistence.createTransaction();
        try {
            EntityManager em = persistence.getEntityManager();
            Deployment deployment = repositoryService.createDeployment()
                    .addString("process.bpmn20.xml", xml)
                    .disableSchemaValidation()
                    .deploy();
            ProcessDefinition activitiProcessDefinition = repositoryService.createProcessDefinitionQuery()
                    .deploymentId(deployment.getId())
                    .singleResult();
            if (procDefinition == null) {
                procDefinition = metadata.create(ProcDefinition.class);
                String code = evaluateProcDefinitionCode(activitiProcessDefinition.getKey());
                procDefinition.setCode(code);
            } else {
                procDefinition = em.reload(procDefinition);
                if (procDefinition == null)
                    throw new BpmException("Error when deploying process. Process definition has been removed");
                processMigrator.migrate(activitiProcessDefinition);
                processMigrator.migrateProcTasks(procDefinition, activitiProcessDefinition.getId());
            }
            procDefinition.setName(activitiProcessDefinition.getName());
            procDefinition.setActId(activitiProcessDefinition.getId());
            procDefinition.setModel(procModel);
            procDefinition.setActive(true);
            procDefinition.setDeploymentDate(timeSource.currentTimestamp());
            em.persist(procDefinition);

            List<ProcRole> procRoles = syncProcRoles(procDefinition);
            procDefinition.setProcRoles(procRoles);

            tx.commit();
            return procDefinition;
        } catch (XMLException e) {
            String msg = "Error on model deployment";
            if (e.getMessage().contains("Attribute 'sourceRef' must appear on element")) {
                msg = "Model elements are not linked properly";
            }
            throw new InvalidModelException(msg, e);
        } catch (Exception e) {
            throw new InvalidModelException(e.getMessage(), e);
        }
        finally {
            tx.end();
        }

    }
}
