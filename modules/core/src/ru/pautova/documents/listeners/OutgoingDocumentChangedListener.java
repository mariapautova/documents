package ru.pautova.documents.listeners;

import com.haulmont.cuba.core.app.events.EntityChangedEvent;
import com.haulmont.cuba.core.global.DataManager;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.documents.entity.OutgoingDocument;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.haulmont.cuba.core.app.events.EntityChangedEvent.Type.UPDATED;

@Component("documents_OutgoingDocumentChangedListener")
public class OutgoingDocumentChangedListener {

    @Inject
    DataManager dataManager;

    @EventListener
    @Transactional
    public void beforeCommit(EntityChangedEvent<OutgoingDocument, UUID> event) {
        if (event.getType().equals(UPDATED)) {
            OutgoingDocument document = dataManager.load(OutgoingDocument.class)
                    .id(event.getEntityId()
                            .getValue())
                    .one();
            document.setEditedDate(LocalDateTime.now());
        }
    }
}