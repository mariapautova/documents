package ru.pautova.documents.listeners;

import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.documents.entity.OutgoingDocument;

import java.time.LocalDateTime;
import java.util.UUID;

@Component("documents_BPMEventListener")
public class BPMEventListener implements ExecutionListener {

    @EventListener
    @Transactional
    @Override
    public void notify(DelegateExecution execution) {
        String currentActivityName = execution.getCurrentActivityName();

        if (currentActivityName.equals("start")) {
            TransactionalDataManager txDm = AppBeans.get(TransactionalDataManager.class);
            OutgoingDocument editedEntity = getEditedEntity(execution);
            editedEntity.setTaskStartDate(LocalDateTime.now());
            txDm.save(editedEntity);
        }
    }

    private OutgoingDocument getEditedEntity(DelegateExecution execution) {
        DataManager dm = AppBeans.get(DataManager.class);

        RuntimeService runtimeService = execution.getEngineServices().getRuntimeService();
        String executionId = execution.getId();
        UUID entityId = (UUID) runtimeService.getVariable(executionId, "entityId");

        return dm.load(OutgoingDocument.class).id(entityId).one();
    }
}
