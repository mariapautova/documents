package ru.pautova.documents.listeners;

import com.haulmont.bpm.entity.ProcInstance;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.entity.ReferenceToEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.security.entity.User;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;
import ru.pautova.documents.entity.OutgoingDocument;
import ru.pautova.documents.entity.Worker;
import ru.pautova.documents.exception.DocumentationException;
import ru.pautova.documents.service.DocumentStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Component("documents_BPMTaskListener")
public class BPMTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        String currentActivityName = delegateTask.getName();

        if (currentActivityName.equals("Approval") || currentActivityName.equals("Registration and sending document")) {
            Persistence persistence = AppBeans.get(Persistence.class);
            EntityManager em = persistence.getEntityManager();
            DataManager dm = AppBeans.get(DataManager.class);
            TransactionalDataManager txDm = AppBeans.get(TransactionalDataManager.class);

            String processInstanceId = delegateTask.getProcessInstanceId();

            ProcInstance procInstance = em.createQuery("select pi from bpm$ProcInstance pi where " +
                            "pi.actProcessInstanceId = :processInstanceId", ProcInstance.class)
                    .setParameter("processInstanceId", processInstanceId)
                    .getSingleResult();
            ReferenceToEntity referenceToEntity = procInstance.getEntity();
            UUID entityId = referenceToEntity.getEntityId();

            LoadContext<OutgoingDocument> outgoingDocumentLoadContext = LoadContext.create(OutgoingDocument.class).setQuery(
                            LoadContext.createQuery("select od from documents_OutgoingDocument as od where od.id = :id")
                                    .setParameter("id", entityId))
                    .setView("outgoingDocument-editView");
            OutgoingDocument outgoingDocument = dm.load(outgoingDocumentLoadContext);

            if (currentActivityName.equals("Approval")) {
                String assignee = delegateTask.getAssignee();
                User user = dm.load(User.class).id(UUID.fromString(assignee)).one();
                Worker approver = getWorkerByUser(user);
                if (approver != null) {
                    outgoingDocument.setApprover(approver);
                    txDm.save(outgoingDocument);
                }
            }
            if (currentActivityName.equals("Registration and sending document")) {
                LoadContext<OutgoingDocument> context = LoadContext.create(OutgoingDocument.class).setQuery(
                        LoadContext.createQuery("select od from documents_OutgoingDocument as od"));
                long count = dm.getCount(context);
                String number = String.valueOf(count + 1);

                outgoingDocument.setRegistrationNumber(number);
                outgoingDocument.setStatus(DocumentStatus.REGISTERED.toString());
                outgoingDocument.setTaskCompletionDate(LocalDateTime.now());
                outgoingDocument.setRegistrationDate(LocalDateTime.now());

                String formatDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                String name = outgoingDocument.getType().getName() +
                        " № " +
                        number +
                        " от " +
                        formatDate +
                        " в " +
                        outgoingDocument.getAddresseeCompany() +
                        ", " +
                        outgoingDocument.getSubject();
                outgoingDocument.setName(name);

                txDm.save(outgoingDocument);
            }
        }
    }

    private Worker getWorkerByUser(User user) {
        DataManager dm = AppBeans.get(DataManager.class);

        LoadContext<Worker> workerContext = LoadContext.create(Worker.class).setQuery(
                        LoadContext.createQuery("select w from documents_Worker as w where w.user = :user")
                                .setParameter("user", user))
                .setView("worker-editView");
        Worker worker = dm.load(workerContext);
        if (worker == null) {
            throw new DocumentationException("approver.notFound");
        } else {
            return dm.load(workerContext);
        }
    }
}