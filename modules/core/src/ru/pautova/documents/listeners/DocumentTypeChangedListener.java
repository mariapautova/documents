package ru.pautova.documents.listeners;

import com.haulmont.cuba.core.app.events.EntityPersistingEvent;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.pautova.documents.entity.DocumentType;

import javax.inject.Inject;

@Component("documents_DocumentTypeChangedListener")
public class DocumentTypeChangedListener {

    @Inject
    private DataManager dataManager;

    @EventListener
    @Transactional
    public void beforePersist(EntityPersistingEvent<DocumentType> event) {

        LoadContext<DocumentType> context = LoadContext.create(DocumentType.class).setQuery(
                LoadContext.createQuery("select d from documents_DocumentType as d"));
        long count = dataManager.getCount(context);
        String number = String.valueOf(count + 1);

        DocumentType documentType = event.getEntity();
        documentType.setCode("ВД00000" + number);
    }
}