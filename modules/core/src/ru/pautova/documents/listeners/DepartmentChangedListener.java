package ru.pautova.documents.listeners;

import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.app.events.AttributeChanges;
import com.haulmont.cuba.core.app.events.EntityChangedEvent;
import com.haulmont.cuba.core.app.events.EntityPersistingEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pautova.documents.entity.Department;
import ru.pautova.documents.exception.DocumentationException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.haulmont.cuba.core.app.events.EntityChangedEvent.Type.UPDATED;

@Component("documents_DepartmentChangedListener")
public class DepartmentChangedListener {

    @Inject
    TransactionalDataManager transactionalDataManager;

    @EventListener
    public void beforePersist(EntityPersistingEvent<Department> event) {
        Department department = event.getEntity();
        Department leadingDepartment = department.getLeadingDepartment();
        if (leadingDepartment != null) {
            loopingCheck(leadingDepartment, new ArrayList<>());
        }
    }

    @EventListener
    public void beforeCommit(EntityChangedEvent<Department, UUID> event) {
        if (event.getType().equals(UPDATED)) {
            AttributeChanges changes = event.getChanges();
            if (changes.isChanged("leadingDepartment")) {
                Department department = transactionalDataManager.load(Department.class)
                        .view("department-editView")
                        .id(event.getEntityId()
                                .getValue())
                        .one();
                Department leadingDepartment = department.getLeadingDepartment();
                if (leadingDepartment != null) {
                    loopingCheck(leadingDepartment, new ArrayList<>());
                }
            }
        }
    }


    private void loopingCheck(Department possLeadDepartment, List<Department> departmentList) {
        Department leadingDepartment = possLeadDepartment.getLeadingDepartment();
        if (leadingDepartment != null) {
            departmentList.forEach(d -> {
                if (d == possLeadDepartment) {
                    throw new DocumentationException("Department looping");
                }
            });
            departmentList.add(possLeadDepartment);
            loopingCheck(leadingDepartment, departmentList);
        }
    }
}