package ru.pautova.documents.service;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import java.util.Arrays;

public enum DocumentStatus implements EnumClass<String> {
    NEW("NEW"),
    APPROVAL("APPROVAL"),
    SIGNING("SIGNING"),
    REFINEMENT("REFINEMENT"),
    REGISTERED("REGISTERED");

    String id;

    DocumentStatus(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public static DocumentStatus fromId(String name) {
        return Arrays.stream(DocumentStatus.values())
                .filter(val -> val.getId().equals(name))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
