package ru.pautova.documents.exception;

public class DocumentationException extends RuntimeException {

    public DocumentationException(Throwable cause) {
        super(cause);
    }

    public DocumentationException(String message) {
        super(message);
    }
}
