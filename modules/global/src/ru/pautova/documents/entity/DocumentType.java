package ru.pautova.documents.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.PublishEntityChangedEvents;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@PublishEntityChangedEvents
@Table(name = "DOCUMENTS_DOCUMENT_TYPE")
@Entity(name = "documents_DocumentType")
@NamePattern("%s|name")
public class DocumentType extends StandardEntity {
    private static final long serialVersionUID = -4389922736180183642L;

    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}