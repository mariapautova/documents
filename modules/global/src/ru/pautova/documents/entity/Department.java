package ru.pautova.documents.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.PublishEntityChangedEvents;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@PublishEntityChangedEvents
@Table(name = "DOCUMENTS_DEPARTMENT")
@Entity(name = "documents_Department")
@NamePattern("%s|name")
public class Department extends StandardEntity {
    private static final long serialVersionUID = 2460947944031506429L;

    @NotNull
    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LEADING_DEPARTMENT_ID")
    private Department leadingDepartment;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HEAD_ID")
    private Worker head;

    public Worker getHead() {
        return head;
    }

    public void setHead(Worker head) {
        this.head = head;
    }

    public Department getLeadingDepartment() {
        return leadingDepartment;
    }

    public void setLeadingDepartment(Department leadingDepartment) {
        this.leadingDepartment = leadingDepartment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}