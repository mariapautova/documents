package ru.pautova.documents.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.entity.annotation.PublishEntityChangedEvents;
import com.haulmont.cuba.security.entity.User;
import ru.pautova.documents.service.DocumentStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@PublishEntityChangedEvents
@Table(name = "DOCUMENTS_OUTGOING_DOCUMENT")
@Entity(name = "documents_OutgoingDocument")
@NamePattern("%s|name")
public class OutgoingDocument extends StandardEntity {
    private static final long serialVersionUID = 7857721530951535200L;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "TYPE_ID")
    private DocumentType type;

    @JoinTable(name = "DOCUMENTS_OUTGOING_DOCUMENT_FILE_DESCRIPTOR_LINK",
            joinColumns = @JoinColumn(name = "OUTGOING_DOCUMENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILE_DESCRIPTOR_ID"))
    @ManyToMany
    private List<FileDescriptor> files;

    @Column(name = "REGISTRATION_NUMBER")
    private String registrationNumber;

    @Column(name = "REGISTRATION_DATE")
    private LocalDateTime registrationDate;

    @Column(name = "ADDRESSEE")
    private String addressee;

    @NotNull
    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    @Column(name = "ADDRESSEE_COMPANY")
    private String addresseeCompany;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXECUTOR_WORKER_ID")
    private Worker executor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIGNATORY_WORKER_ID")
    private Worker signatory;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "NAME")
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "AUTHOR_USER_ID")
    private User author;

    @Column(name = "CREATION_DATE")
    private LocalDateTime creationDate;

    @Column(name = "EDITED_DATE")
    private LocalDateTime editedDate;

    @Column(name = "STATUS")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPROVER_ID")
    private Worker approver;

    @Column(name = "TASK_START_DATE")
    private LocalDateTime taskStartDate;

    @Column(name = "TASK_COMPLETION_DATE")
    private LocalDateTime taskCompletionDate;

    @Column(name = "RESULT_")
    private String result;

    @Column(name = "COMMENT_")
    private String comment;

    public List<FileDescriptor> getFiles() {
        return files;
    }

    public void setFiles(List<FileDescriptor> files) {
        this.files = files;
    }

    public LocalDateTime getTaskCompletionDate() {
        return taskCompletionDate;
    }

    public String getComment() {
        return comment;
    }

    public String getResult() {
        return result;
    }

    public LocalDateTime getTaskStartDate() {
        return taskStartDate;
    }

    public Worker getApprover() {
        return approver;
    }

    public String getStatus() {
        if (this.status == null) {
            return null;
        }
        return DocumentStatus.fromId(this.status).toString();
    }

    public void setStatus(String status) {
        DocumentStatus documentStatus = DocumentStatus.fromId(status);
        this.status = documentStatus.getId();
    }

    public LocalDateTime getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(LocalDateTime editedDate) {
        this.editedDate = editedDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Worker getSignatory() {
        return signatory;
    }

    public void setSignatory(Worker signatory) {
        this.signatory = signatory;
    }

    public Worker getExecutor() {
        return executor;
    }

    public void setExecutor(Worker executor) {
        this.executor = executor;
    }

    public String getAddresseeCompany() {
        return addresseeCompany;
    }

    public void setAddresseeCompany(String addresseeCompany) {
        this.addresseeCompany = addresseeCompany;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public void setApprover(Worker approver) {
        this.approver = approver;
    }

    public void setTaskStartDate(LocalDateTime taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public void setTaskCompletionDate(LocalDateTime taskCompletionDate) {
        this.taskCompletionDate = taskCompletionDate;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}