package ru.pautova.documents.web.screens.documenttype;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.DocumentType;

@UiController("documents_DocumentType.edit")
@UiDescriptor("document-type-edit.xml")
@EditedEntityContainer("documentTypeDc")
@LoadDataBeforeShow
public class DocumentTypeEdit extends StandardEditor<DocumentType> {
}