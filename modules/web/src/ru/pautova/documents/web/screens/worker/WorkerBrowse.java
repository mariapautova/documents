package ru.pautova.documents.web.screens.worker;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.Worker;

@UiController("documents_Worker.browse")
@UiDescriptor("worker-browse.xml")
@LookupComponent("workersTable")
@LoadDataBeforeShow
public class WorkerBrowse extends StandardLookup<Worker> {
}