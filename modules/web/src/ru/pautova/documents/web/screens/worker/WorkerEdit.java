package ru.pautova.documents.web.screens.worker;

import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.security.entity.User;
import ru.pautova.documents.entity.Worker;

@UiController("documents_Worker.edit")
@UiDescriptor("worker-edit.xml")
@EditedEntityContainer("workerDc")
@LoadDataBeforeShow
public class WorkerEdit extends StandardEditor<Worker> {

    @Subscribe(id = "workerDc", target = Target.DATA_CONTAINER)
    public void onWorkerDcItemPropertyChange(InstanceContainer.ItemPropertyChangeEvent<Worker> event) {
        String property = event.getProperty();

        if (property.equals("user")) {
            Worker worker = event.getItem();
            User user = worker.getUser();

            User previousUser = null;
            Object prevValue = event.getPrevValue();
            if (prevValue != null) {
                previousUser = (User) prevValue;
            }

            if (user != null && previousUser != user) {
                worker.setFirstName(user.getFirstName());
                worker.setLastName(user.getLastName());
                if (user.getMiddleName() != null) {
                    worker.setPatronymic(user.getMiddleName());
                }
            }
        }
    }
}