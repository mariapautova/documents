package ru.pautova.documents.web.screens.outgoingdocument;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.OutgoingDocument;

@UiController("documents_OutgoingDocument.browse")
@UiDescriptor("outgoing-document-browse.xml")
@LookupComponent("outgoingDocumentsTable")
@LoadDataBeforeShow
public class OutgoingDocumentBrowse extends StandardLookup<OutgoingDocument> {
}