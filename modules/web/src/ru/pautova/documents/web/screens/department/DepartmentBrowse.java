package ru.pautova.documents.web.screens.department;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.Department;

@UiController("documents_Department.browse")
@UiDescriptor("department-browse.xml")
@LookupComponent("departmentsTable")
@LoadDataBeforeShow
public class DepartmentBrowse extends StandardLookup<Department> {
}