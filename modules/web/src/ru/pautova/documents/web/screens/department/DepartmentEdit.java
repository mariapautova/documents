package ru.pautova.documents.web.screens.department;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.Department;

@UiController("documents_Department.edit")
@UiDescriptor("department-edit.xml")
@EditedEntityContainer("departmentDc")
@LoadDataBeforeShow
public class DepartmentEdit extends StandardEditor<Department> {
}