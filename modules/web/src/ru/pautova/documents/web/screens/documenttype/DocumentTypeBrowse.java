package ru.pautova.documents.web.screens.documenttype;

import com.haulmont.cuba.gui.screen.*;
import ru.pautova.documents.entity.DocumentType;

@UiController("documents_DocumentType.browse")
@UiDescriptor("document-type-browse.xml")
@LookupComponent("documentTypesTable")
@LoadDataBeforeShow
public class DocumentTypeBrowse extends StandardLookup<DocumentType> {
}