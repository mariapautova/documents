package ru.pautova.documents.web.screens.outgoingdocument;

import com.haulmont.bpm.entity.ProcActor;
import com.haulmont.bpm.entity.ProcAttachment;
import com.haulmont.bpm.entity.ProcInstance;
import com.haulmont.bpm.entity.ProcRole;
import com.haulmont.bpm.gui.procactionsfragment.ProcActionsFragment;
import com.haulmont.bpm.service.BpmEntitiesService;
import com.haulmont.cuba.core.entity.FileDescriptor;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.app.core.file.FileDownloadHelper;
import com.haulmont.cuba.gui.components.FileUploadField;
import com.haulmont.cuba.gui.components.HBoxLayout;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.CollectionPropertyContainer;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.model.InstanceLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.upload.FileUploadingAPI;
import com.haulmont.cuba.security.entity.User;
import ru.pautova.documents.entity.Department;
import ru.pautova.documents.entity.OutgoingDocument;
import ru.pautova.documents.entity.Worker;
import ru.pautova.documents.exception.DocumentationException;
import ru.pautova.documents.web.service.OutgoingDocumentContext;

import javax.inject.Inject;
import java.util.*;

@UiController("documents_OutgoingDocument.edit")
@UiDescriptor("outgoing-document-edit.xml")
@EditedEntityContainer("outgoingDocumentDc")
@LoadDataBeforeShow
public class OutgoingDocumentEdit extends StandardEditor<OutgoingDocument> {
    private static final String PROCESS_CODE = "documentApproval";

    @Inject
    private CollectionLoader<ProcAttachment> docprocAttachmentsDl;

    @Inject
    private InstanceContainer<OutgoingDocument> outgoingDocumentDc;

    @Inject
    protected CollectionPropertyContainer<FileDescriptor> filesDc;

    @Inject
    protected ProcActionsFragment procActionsFragment;

    @Inject
    private Table<ProcAttachment> attachmentsTable;

    @Inject
    private InstanceLoader<OutgoingDocument> outgoingDocumentDl;

    @Inject
    protected BpmEntitiesService bpmEntitiesService;

    @Inject
    DataManager dataManager;

    @Inject
    UserSessionSource userSessionSource;

    @Inject
    private Metadata metadata;

    @Inject
    protected HBoxLayout filesWrapperLayout;

    @Inject
    protected FileUploadingAPI fileUploadingAPI;

    @Inject
    protected FileUploadField upload;

    @Inject
    protected Notifications notifications;

    private List<FileDescriptor> newImageDescriptors = new ArrayList<>();


    @Subscribe
    private void onBeforeShow(BeforeShowEvent event) {
        outgoingDocumentDl.load();
        docprocAttachmentsDl.setParameter("entityId", outgoingDocumentDc.getItem().getId());
        docprocAttachmentsDl.load();
        procActionsFragment.initializer()
                .standard()
                .init(PROCESS_CODE, getEditedEntity());

        FileDownloadHelper.initGeneratedColumn(attachmentsTable, "file");
    }

    @Subscribe
    public void onInitEntity(InitEntityEvent<OutgoingDocument> event) {
        UUID userId = userSessionSource.currentOrSubstitutedUserId();
        User currentUser = dataManager.load(User.class)
                .id(userId)
                .one();

        LoadContext<Worker> workerContext = LoadContext.create(Worker.class).setQuery(
                        LoadContext.createQuery("select w from documents_Worker as w where w.user = :user")
                                .setParameter("user", currentUser))
                .setView("worker-editView");
        Worker worker = dataManager.load(workerContext);

        OutgoingDocumentContext.build(event, currentUser, worker);
    }

    @Subscribe
    private void onAfterShow(AfterShowEvent event) {
        User signatory;

        UUID outgoingDocumentId = getEditedEntity().getId();
        LoadContext<OutgoingDocument> outgoingDocumentLoadContext = LoadContext.create(OutgoingDocument.class).setQuery(
                        LoadContext.createQuery("select od from documents_OutgoingDocument as od where od.id = :id")
                                .setParameter("id", outgoingDocumentId))
                .setView("outgoingDocument-editView");
        OutgoingDocument outgoingDocument = dataManager.load(outgoingDocumentLoadContext);

        if (outgoingDocument != null) {
            Worker signatoryWorker = outgoingDocument.getSignatory();
            if (signatoryWorker != null) {
                signatory = signatoryWorker.getUser();
                if (signatory == null) {
                    throw new DocumentationException("signatory.error");
                }
            } else {
                signatory = null;
            }
        } else {
            signatory = null;
        }
        procActionsFragment.initializer()
                .standard()
                .setBeforeStartProcessPredicate(() -> {
                    ProcInstance procInstance = procActionsFragment.getProcInstance();
                    ProcActor initiatorProcActor = createProcActor("initiator", procInstance, currentUser());
                    ProcActor departmentHeadProcActor = createProcActor("departmentHead", procInstance, departmentHead());
                    ProcActor signatoryProcActor = createProcActor("signatory", procInstance, signatory);

                    Set<ProcActor> procActors = new HashSet<>();
                    procActors.add(initiatorProcActor);
                    procActors.add(departmentHeadProcActor);
                    procActors.add(signatoryProcActor);
                    procInstance.setProcActors(procActors);

                    return true;
                })
                .init(PROCESS_CODE, getEditedEntity());
    }

    @Subscribe("upload")
    protected void onUploadFileUploadSucceed(
            FileUploadField.FileUploadSucceedEvent event
    ) {
        FileDescriptor imageDescriptor = upload.getFileDescriptor();

        try {
            fileUploadingAPI.putFileIntoStorage(upload.getFileId(), imageDescriptor);

            FileDescriptor savedImageDescriptor = dataManager.commit(imageDescriptor);
            newImageDescriptors.add(savedImageDescriptor);

            filesDc.getMutableItems().add(savedImageDescriptor);

            notifications.create(Notifications.NotificationType.TRAY)
                    .withCaption("download.ok")
                    .show();
        } catch (FileStorageException e) {
            notifications.create(Notifications.NotificationType.ERROR)
                    .withCaption("download.failed")
                    .show();
        }
    }

    private ProcActor createProcActor(String procRoleCode, ProcInstance procInstance, User user) {
        ProcActor initiatorProcActor = metadata.create(ProcActor.class);
        initiatorProcActor.setUser(user);
        ProcRole initiatorProcRole = bpmEntitiesService.findProcRole(PROCESS_CODE, procRoleCode, View.MINIMAL);
        initiatorProcActor.setProcRole(initiatorProcRole);
        initiatorProcActor.setProcInstance(procInstance);
        return initiatorProcActor;
    }

    private User currentUser() {
        UUID userId = userSessionSource.currentOrSubstitutedUserId();
        return dataManager.load(User.class)
                .id(userId)
                .one();
    }

    private User departmentHead() {
        LoadContext<Worker> workerContext = LoadContext.create(Worker.class).setQuery(
                        LoadContext.createQuery("select w from documents_Worker as w where w.user = :user")
                                .setParameter("user", currentUser()))
                .setView("worker-editView");

        Worker currentWorker = dataManager.load(workerContext);
        if (currentWorker == null) {
            throw new DocumentationException("worker.error");
        }
        Department department = currentWorker.getDepartment();
        if (department == null) {
            throw new DocumentationException("department.error");
        }
        Worker departmentHeadWorker = department.getHead();
        if (departmentHeadWorker == null) {
            throw new DocumentationException("departmentHead.error");
        }
        User departmentHead = departmentHeadWorker.getUser();
        if (departmentHead == null) {
            throw new DocumentationException("departmentHead.user.error");
        }
        return departmentHead;
    }
}