package ru.pautova.documents.web.service;

import com.haulmont.cuba.gui.screen.StandardEditor;
import com.haulmont.cuba.security.entity.User;
import ru.pautova.documents.entity.OutgoingDocument;
import ru.pautova.documents.entity.Worker;
import ru.pautova.documents.service.DocumentStatus;

import java.time.LocalDateTime;

public class OutgoingDocumentContext {

    public static void build(StandardEditor.InitEntityEvent<OutgoingDocument> event, User currentUser,
                             Worker worker) {
        OutgoingDocument document = event.getEntity();

        document.setAuthor(currentUser);
        document.setExecutor(worker);
        document.setCreationDate(LocalDateTime.now());
        document.setEditedDate(LocalDateTime.now());
        document.setStatus(DocumentStatus.NEW.toString());
    }
}