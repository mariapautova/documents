### Document approval application

* Шаблон отчета по зарегистрированным документам для импорта находится в файле "Registered documents report.zip"
* Шаблон модели процесса согласования документа для импорта находится в файле "Document Approval.json"
* Схема модели для иллюстрации приложена в файле "Document Approval BPM.jpg"
